(ns quiz.core
  (:gen-class)
  (:require [net.cgrand.enlive-html :as html]
            [ring.adapter.jetty :refer [run-jetty]]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [compojure.route :as route]
            [compojure.handler :as handler]
            [compojure.response :as response]
            [net.cgrand.reload]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [clojure.data.json :as json])
  (:use compojure.core
        ))
(net.cgrand.reload/auto-reload *ns*)
(def questions (:arr (json/read-str (slurp "./resources/pytania.json") :key-fn keyword)))
(def images ["bocian" "kot" "pies" "ptak1" "ptak2" "squirrel"])
(def categories 
  [
   {:name "Symbole i Legendy" :image "cat1" :color "518faf"}
   {:name "Historia" :image "cat2" :color "3e8c68"}
   {:name "Architektura" :image "cat3" :color "ba2b43"}
   {:name "Gwara" :image "cat4" :color "564747" }
   {:name "Podróże" :image "cat5" :color "edbe54" }
   {:name "Warszawa wielokultowa" :image "cat6" :color "eb9686" }
   {:name "Różne" :image "cat7" :color "6f519e"}
   {:name "Inne" :image "cat8" :color "ce4f3a"}
   ])

(defn categories-with-questions []
  (let [qu questions
        cat categories]
    (defn iter [cate q ret]
      (if (empty? cate)
        ret
        (recur (rest cate) (drop 4 q) (concat ret [(assoc (first cate) :questions (take 4 questions))]))))
    (iter cat qu [])) )

(html/deftemplate layout "layout.html" [content]
  [:body] (html/content content))

(html/defsnippet game-confirm "game_confirm.html" [:#game-confirm] [])

(html/defsnippet index "index.html" [:#main] [players categories game-confirm game-question]
  [:.player-selection] (html/content players)
  [:.categories-selection] (html/content categories)
  [:.game-confirm] (html/content game-confirm)
  [:.game-main] (html/content game-question))

(html/defsnippet player-item "player_selection.html" [:.player-conainter :> html/first-child]
  [image-url]
  [:img] (html/set-attr :src (str "images/" image-url ".png")))

(html/defsnippet category-item "categories_selection.html" [:.categories-conainter :> html/first-child]
  [{:keys [name image color questions]}]
  [:.category-item] (html/do-> 
                             (html/set-attr :style (str "background-color: #" color))
                             (html/set-attr :data-name name)
                             (html/set-attr :data-questions (json/write-str questions)))
  [:img] (html/set-attr :src (str "images/categories/" image ".png"))
)
(html/defsnippet player-selection "player_selection.html" [:#player-selection] []
  [:.player-conainter] (html/content (map #(player-item %) images))) 

(html/defsnippet categories-selection "categories_selection.html" [:#category-selection] []
  [:.categories-conainter] (html/content (map #(category-item %) (categories-with-questions))))

(html/defsnippet game-question "game_question.html" [:#game-question] [])

(defn about []
  (layout (index (player-selection) (categories-selection) (game-confirm) (game-question))))

(defroutes app-routes
  (GET "/" [] (about))
  (route/resources "/")
  (route/not-found "<h1>notfound</h1>"))

(def app
  (wrap-defaults  app-routes site-defaults))
(defonce ^:dynamic *server* (run-jetty app {:port 3000 :join? false}))
(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))
