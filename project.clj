(defproject quiz "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [ring "1.6.1"]
                 [compojure "1.6.0"]
                 [enlive "1.1.6"]
                 [ring/ring-defaults "0.2.1"]
                 [org.clojure/data.json "0.2.6"]]
  :main ^:skip-aot quiz.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  :plugins [[lein-ring "0.12.0"]]
  :ring {:handler quiz.core/app}
  )
